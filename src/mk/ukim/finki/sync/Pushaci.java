package mk.ukim.finki.sync;

import java.util.concurrent.Semaphore;

public class Pushaci {

    static Semaphore accessBuffer;
    static Semaphore getItemsSem;
    static Semaphore wait[];
    static boolean waiting[];

    static void init() {
        accessBuffer = new Semaphore(1);
        getItemsSem = new Semaphore(0);
        wait = new Semaphore[3];
        waiting = new boolean[3];
        for (int i=0;i<3;i++) {
            wait[i] = new Semaphore(0);
        }
    }

    public static class Masa{

        private int type;

        public void putItems() {
            this.type = (int)Math.round(Math.random()*3);
        }

        public void consume(int type) {
            System.out.println("Consuming...");
        }

        public boolean hasMyItems(int type) {
            return (this.type==type);
        }

    }

    public static class Agent extends Thread{

        private Masa m = null;

        public Agent(Masa m) {
            this.m = m;
        }

        public void execute() throws InterruptedException {
            accessBuffer.acquire();
            this.m.putItems();
            for (int i=0;i<3;i++) {
                if (waiting[i]==true) {
                    waiting[i]=false;
                    wait[i].release();
                }
            }
            getItemsSem.release();

        }

        @Override
        public void run() {
        }
    }

    public static class Pushac extends Thread{

        private int type;
        private Masa m;
        public Pushac(int type, Masa m) {
            this.type = type;
            this.m = m;
        }

        public void execute() throws InterruptedException {
            getItemsSem.acquire();
            if (this.m.hasMyItems(type)) {
                this.m.consume(type);
                accessBuffer.release();
            } else {
                waiting[type]=true;
//                getItemsSem.release();
                wait[type].acquire();


            }
        }

        @Override
        public void run() {
            super.run();
        }
    }
}
