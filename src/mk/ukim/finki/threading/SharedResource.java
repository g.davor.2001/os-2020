package mk.ukim.finki.threading;

public class SharedResource {

    private static int counter = 0;

    public void increment() {
        synchronized (SharedResource.class) {
            int newCounter = counter;
            newCounter++;
            System.out.print("");
            counter = newCounter;
        }
        System.out.println(counter);
    }
}
