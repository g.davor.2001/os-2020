package mk.ukim.finki.threading;

public class SequenceIncrementThread extends Thread {

    private SharedResource resource;

    public SequenceIncrementThread(SharedResource resource) {
            this.resource = resource;
    }

    @Override
    public void run() {
        for (int i=0;i<50000;i++) {
            resource.increment();
        }
    }
}
