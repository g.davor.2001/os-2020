package mk.ukim.finki.threading.matrix;

import java.util.Scanner;

public class Matrix {

    int m;
    int n;

    double[][] matrix;

    public Matrix(int m, int n, double[][] matrix) {
        this.m = m;
        this.n = n;
        this.matrix = matrix;
    }

    public Matrix(int m, int n) {
        this.m = m;
        this.n = n;
        this.matrix = new double[m][n];
    }



    public void setEl(int i, int j, double value) {
        matrix[i][j] = value;
    }

    public double[] getRow(int i) {
        return matrix[i];
    }

    public double[] getColumn(int c) {
        double[] column = new double[m];
        for (int i=0;i<m;i++) {
            column[i] = matrix[i][c];
        }
        return column;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(double[][] matrix) {
        this.matrix = matrix;
    }

    public void print() {
        for (int i=0;i<m;i++) {
            for (int j=0;j<n;j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static Matrix buildFromStdIn() {

        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        Matrix matrix = new Matrix(m,n);
        for (int i=0;i<m;i++) {
            for (int j=0;j<n;j++) {
                System.out.printf("M[%d,%d]=",i,j);
                double value = scanner.nextDouble();
                matrix.setEl(i,j,value);
            }
        }
        return matrix;
    }
}
